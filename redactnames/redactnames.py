# Web libraries
import http.server
import socketserver
import requests
import json

# Named Entity Recognition library and model
import spacy
import en_core_web_sm

# HTML parser for content_html (Spacy does not support HTML string)
from bs4 import BeautifulSoup

PORT = 8000
DATA_URL = "http://therecord.co/feed.json"

# Create NLP object first to imporve performance
nlp = en_core_web_sm.load(disable = ['tagger', 'parser'])

class APIRequestHandler(http.server.SimpleHTTPRequestHandler):
    def _set_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'application/json')
        self.end_headers()

    def do_HEAD(self):
        self._set_headers()

    def do_GET(self):
        data = get_data()
        self._set_headers()
        self.wfile.write(redact(data))

def get_data():
    print(f"Getting data from the original source ({DATA_URL})")
    data = requests.get(DATA_URL)

    return data.json()

def redact_string(string):
    doc = nlp(string)
    redacted_sentences = []

    for ent in doc.ents:
        ent.merge()

    for token in doc:
        if token.ent_type_ == 'PERSON':
            redacted_sentences.append('[REDACTED] ')
        else:
            redacted_sentences.append(token.string)

    return "".join(redacted_sentences)

def redact_html(html_string):
    soup = BeautifulSoup(html_string, features='html.parser')

    for text in soup.findAll(text=True):
        redacted_text = redact_string(str(text))
        text.replaceWith(redacted_text)

    return str(soup)

def redact(data):
    print("Redacting, please wait...")
    data['user_comment'] = redact_string(data['user_comment'])
    data['title'] = redact_string(data['title'])
    data['author']['name'] = redact_string(data['author']['name'])

    for item in data['items']:
        item['title'] = redact_string(item['title'])
        item['content_html'] = redact_html(item['content_html'])

    data['redacted'] = True

    return json.dumps(data).encode(encoding='utf-8')

def main():
    print("Starting API server...")
    httpd = http.server.HTTPServer(('', PORT), APIRequestHandler)
    httpd.serve_forever()

if __name__ == "__main__":
    main()

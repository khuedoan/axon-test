# Axon homework

[![pipeline status](https://gitlab.com/khuedoan/axon-test/badges/master/pipeline.svg)](https://gitlab.com/khuedoan/axon-test/-/commits/master)

## Contents

<!-- vim-markdown-toc GitLab -->

* [Features](#features)
    * [Name redaction service](#name-redaction-service)
    * [Package and deploy](#package-and-deploy)
    * [CI/CD](#cicd)
* [Handle zero-downtime upgrades of the service](#handle-zero-downtime-upgrades-of-the-service)
* [Usage](#usage)
    * [Docker Compose](#docker-compose)
    * [Kubernetes](#kubernetes)
* [What can be improved](#what-can-be-improved)
* [References](#references)

<!-- vim-markdown-toc -->

## Features

### Name redaction service

`./redactnames/redactnames.py`

A simple Python HTTP server, with the following logic:

1. Run an HTTP server on port `8000`
2. Get a `GET` request from user
3. Get original JSON data from http://therecord.co/feed.json
4. Redact all occurrences of names of people in the following fields using [spaCy](https://spacy.io/), a natural language processing library:
    - `user_comment`
    - `title`
    - `author` `name`
    - `items` `title`
    - `items` `content_html`
5. Add a `'redacted': true` field to the result JSON
6. Return the result JSON document to the client

We need to redact each field independently because the spaCy library does not support HTML string, so the `content_html` field needs special treatment:

1. Parse the HTML content
2. Iterate over all embedded text and redact names

This process takes around 8 seconds (including around 3 seconds to get the original data).
The result replaces around 230 names with `[REDACTED]`, but it will miss some names because of the pre-trained model.

### Package and deploy

- The service is packaged as a Docker container (`./redactnames/Dockerfile`)
- Deploy to Minikube/GKE using `kubectl`

### CI/CD

Using Gitlab CI, with a simple `.gitlab-ci.yml` included in the repository.

How I would implement CI for my service:

1. Build
    - Build your applications/images
2. Test
    - Code coverage
    - Run unit test (for Python it could be `unittest`)
    - Run API test (simple `curl`)
3. Deploy to stagging
    - Push your images to container registry
    - Deploy your updated application (using `kubectl`, `helm` or Spinnaker)
4. Run test on stagging
    - Run integration test
    - Run component test
    - Run end to end test
    - Run performance test
5. Deploy to production (same as stagging but different environment)
6. Run test on production (same as stagging but different environment)

## Handle zero-downtime upgrades of the service

Using Blue Green deployment strategy:

1. Deploy on a whole new stack
2. Validate
3. Cutover traffic

Or we can use Rolling Red Black, which is the same as the above but we define the percentage to cut over.

We can use Kubernetes `rollout` with environment variables for this strategy.

To implement it we can write scripts in `.gitlab-ci.yml` or use Spinnaker.

## Usage

### Docker Compose

Start the service:

```sh
docker-compose up --build
```

Test if it's working (this takes around 8 seconds):

```sh
curl localhost:8000
```

You can use http://www.jsondiff.com/ to compare the result with the original JSON.

### Kubernetes

Can be used with Minikube or GKE

Build and push the image:

```sh
docker build -t $DOCKER_HUB_USERNAME/axon-test-redactnames ./redactnames/
docker push $DOCKER_HUB_USERNAME/axon-test-redactnames
```

Enable minikube Ingress controller:

```sh
minikube addons enable ingress
```

Deploy with kubectl:

```sh
kubectl create -f ./kubernetes/
# Or use minikube's kubectl
# minikube kubectl -- apply -f ./kubernetes/
```

Get the service url:

```sh
minikube service redactnames --url
```

## What can be improved

A few things that can be improved but I can't do it yet because of the time constraint:

- Performance: improve name redaction speed on the `content_html` field by writing a custom tokenizer instead of iterating through every element, using spaCy's `load.pipe()` and use caching
- Improve person name detection: the default model miss a lot of names
- Improve text replacement: currently it adds a space after `[REDACTED]`, so `Hello Kelvin!` will become `Hello [REDACTED] !` (notice the unnecessary space)
- Use a proper web framework like Flask instead of the simple `http.server`
- Testing: write actual unit test instead of simple function call and `curl`
- Docker image tag: add version tag for the name reaction Docker image
- Implement zero-downtime upgrades

## References

- https://spacy.io/usage/models
- https://stackoverflow.com/questions/53870599/disabling-part-of-the-nlp-pipeline
- https://stackoverflow.com/questions/830997/using-beautiful-soup-how-do-i-iterate-over-all-embedded-text
- https://microservices.io/testing/

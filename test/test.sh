#!/bin/sh

set -e

# Start the service with docker-compse
docker-compose up -d

# Check if we can redact a simple HTML document
docker exec redactnames \
    python -c 'import redactnames; print(redactnames.redact_html("<p>Kelvin is here</p>"))' | \
    grep -F "<p>[REDACTED] is here</p>"

# Check if we can use the service
docker exec redactnames \
    curl localhost:8000

# Stop the service
docker-compose down
